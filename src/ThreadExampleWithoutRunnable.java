
class ShivamAndAmleshTask extends Thread {
	public void run() {
		String name=Thread.currentThread().getName();
		for(int i=1; i<=10; i++) {
			System.out.println("2 x "+i+" = "+(2*i)+" => "+name);
		}
		//System.out.println(10/0);
	}
}



public class ThreadExampleWithoutRunnable {

	public static void main(String[] args) throws Exception {

		System.out.println("Main Thread Started....");
		ShivamAndAmleshTask shivam=new ShivamAndAmleshTask(); shivam.setName("shivam-thread");
		ShivamAndAmleshTask amlesh=new ShivamAndAmleshTask(); amlesh.setName("amlesh-thread");
		ShivamAndAmleshTask manoj=new ShivamAndAmleshTask(); manoj.setName("manoj-thread");
		shivam.start();
		amlesh.start();
		manoj.start();
		shivam.join();
		amlesh.join();
		manoj.join();
		System.out.println("Main Thread Ending...");
	}

}
