class Chat {
	
	boolean flag=false;
	
	public synchronized void question(String msg) {

		if(flag==true) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Question : "+msg);
		flag=true;
		notify();
		
	}

	public synchronized void answer(String msg) {
		if(!flag) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Answer : "+msg);
		flag=false;
		notify();
	}

}



public class InterThreadComExample {

	public static void main(String[] args) {
		
		Chat chat=new Chat();
		String questions[]= {"What Is JDK?","What Is SQL?","What Is JVM?"};
		String answers[]= {"Java Development Kit","Structured Query Language","Java Virtual Machine"};
		
		Thread t1=new Thread(()->{
			for(String question:questions) {
				chat.question(question);
			}
		});
		
		Thread t2=new Thread(()->{
			for(String answer:answers) {
				chat.answer(answer);
			}
		});
		
		t1.start();
		t2.start();
		
		

	}

}
