import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

class FileOperations {
	private String fileName;
	public FileOperations(String fileName) {
		this.fileName=fileName;
	}
	public void readFile() {
		try {
		BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
		while(true) {
			String line=reader.readLine();
			if(line==null)break;
			System.out.println("Line From : "+fileName+" => "+line);
		}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
public class Test {
	public static void main(String[] args) {

		Thread t1=new Thread(new FileOperations("d:/data/info.txt")::readFile);
		Thread t2=new Thread(new FileOperations("d:/data/trial.txt")::readFile);
		t1.start();
		t2.start();
	}
}
