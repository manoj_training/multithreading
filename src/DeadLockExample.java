
public class DeadLockExample {

	public static void main(String[] args) {

		String s1 = "INDORE";
		String s2 = "BHOPAL";

		Thread t1 = new Thread(() -> {
			synchronized (s1) {
				System.out.println("S1 Locked By T1...!");
				for (int i = 0; i < s1.length(); i++) {
					System.out.println(s1.charAt(i));
				}
				
				synchronized(s2) {
					System.out.println("S2 Locked By T1...!");
					for (int i = 0; i < s2.length(); i++) {
						System.out.println(s2.charAt(i));
					}
					
				}
			}
		});

		Thread t2 = new Thread(() -> {
			synchronized (s2) {
				System.out.println("S2 Locked By T2...!");
				for (int i = 0; i < s2.length(); i++) {
					System.out.println(s2.charAt(i));
				}
				
				synchronized(s1) {
					System.out.println("S1 Locked By T2....!");
					for (int i = 0; i < s1.length(); i++) {
						System.out.println(s1.charAt(i));
					}
					
				}
			}
		});

		t1.start();
		t2.start();

	}

}
