
class TaskOne {
	public void addEvenNumbers(){
		int sum=0;
		for(int i=1; i<=5000; i++) {
			try {
			Thread.sleep(1);
			}catch(Exception e) {e.printStackTrace();}
			if(i%2==0)
			sum=sum+i;
		}
		//System.out.println(10/0);
		System.out.println("Even Number Addition : "+sum);
	}
}

class TaskTwo {
	public void addOddNumbers(){
		int sum=0;
		for(int i=1; i<=5000; i++) {
			try {
				Thread.sleep(1);
				}catch(Exception e) {e.printStackTrace();}
			if(i%2!=0)
			sum=sum+i;
		}
		System.out.println("Odd Number Addition : "+sum);
	}	
}

class TaskThree {
	public void addAllNumbers(){
		int sum=0;
		for(int i=1; i<=5000; i++) {
			try {
				Thread.sleep(1);
				}catch(Exception e) {e.printStackTrace();}
			sum=sum+i;
		}
		System.out.println("All Number Addition : "+sum);
	}
}

public class MultiThreadingExample {
	public static void main(String[] args) throws Exception {
		System.out.println("Start Counting...");
		long start,end;
		start=System.currentTimeMillis();
		//multi-threaded-code
		Thread t1=new Thread(new TaskOne()::addEvenNumbers);
		Thread t2=new Thread(new TaskTwo()::addOddNumbers);
		Thread t3=new Thread(new TaskThree()::addAllNumbers);
		t1.start();
		t2.start();
		t3.start();
		t1.join();t2.join();t3.join();
		
		/*
		//single-threaded-code
		TaskOne task1=new TaskOne();
		TaskTwo task2=new TaskTwo();
		TaskThree task3=new TaskThree();
		task1.addEvenNumbers();
		task2.addOddNumbers();
		task3.addAllNumbers();
		*/
		end=System.currentTimeMillis();
		System.out.println("Start Time : "+start);
		System.out.println("End Time : "+end);
		System.out.println("Total Time : "+(end-start));
		System.out.println("Counting Ends...");
	}
	
}
