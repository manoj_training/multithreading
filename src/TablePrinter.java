
public class TablePrinter {

	private static Table table = new Table();

	public static void main(String[] args) {

		Thread t1 = new Thread(() -> {
			synchronized (table) {
				table.printTable(2);
			}
			System.out.println("Here Lock Will Released...");
		});
		Thread t2 = new Thread(() -> {
			synchronized (table) {
				table.printTable(3);
			}
		});
		t1.start();
		t2.start();
	}

}
