
public class BankingOperations {

	static Account ac1=new Account(111,10000);
	static Account ac2=new Account(112,20000);
	static Account ac3=new Account(113,30000);

	public static void main(String[] args) {
		
		Thread t1=new Thread(()->{
			ac1.withdraw(8000);
		}); t1.setName("Shivam");
		
		Thread t2=new Thread(()->{
			ac2.withdraw(15000);
		}); t2.setName("Amlesh");

		Thread t3=new Thread(()->{
			ac1.withdraw(4000);
		}); t3.setName("Shivam's-Brother");

		t1.start();
		t2.start();
		t3.start();
		
		/*
		Account ac1=new Account(111,10000);
		Account ac2=new Account(112,20000);
		Account ac3=new Account(113,30000);
		ac1.withdraw(8000);
		ac2.withdraw(15000);
		ac3.withdraw(21000);
		*/

	}

}
