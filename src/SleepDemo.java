import java.util.concurrent.TimeUnit;

public class SleepDemo {

	public static void main(String[] args) throws Exception {
		
		System.out.println("Starting The Task.....");
		Thread t1=new Thread(()->{
			for(int i=1;i<=5;i++) {
				System.out.println("2 x "+i+" = "+(2*i)+ " => "+Thread.currentThread().getName());
			}
		});
		Thread t2=new Thread(()->{
			for(int i=1;i<=5;i++) {
				System.out.println("3 x "+i+" = "+(3*i)+ " => "+Thread.currentThread().getName());
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		t1.setName("TwoTableThread");
		t2.setName("ThreeTableThread");
		
		t1.start();
		t2.start();
		
		//Thread.sleep(5000);
		
		//TimeUnit.MINUTES.sleep(1);
		Thread.sleep(1000*60*60*60*24*7);
		TimeUnit.DAYS.sleep(3);
		System.out.println("Completed The Task.....!");

	}

}
