
public class Account {
 
	private int ano;
	private int balance;
	
	public Account(int ano, int balance) {this.ano=ano; this.balance=balance;}
	
	public synchronized void withdraw(int amount) {
		String name=Thread.currentThread().getName();
		if(balance>=amount) {
			System.out.println("Transaction  Allowed For "+ano+" = >"+name);
			System.out.println("Collect Cash "+amount+" From Account "+ano+" = >"+name);
			balance=balance-amount;
			System.out.println("Remaining Balance In "+ano+" Is "+balance+" = >"+name);
		}else {
			System.out.println("Insufficient Balance In "+ano+" = >"+name);
		}
		
		System.out.println("Thanks For Using Our Service.");
		System.out.println("Your Balance Now Is : "+balance);
	}
	
}


