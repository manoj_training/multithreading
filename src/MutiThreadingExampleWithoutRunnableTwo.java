class ShivamTask extends Thread {
	public void run() {
		for(int i=1; i<=10; i++) {
			System.out.println("Reading Line #"+i+" From File By "+Thread.currentThread().getName());
		}
	}
}

class AmleshTask extends Thread {
	public void run() {
		for(int i=1; i<=10; i++) {
			System.out.println("Uploading Record #"+i+" To Database By"+Thread.currentThread().getName());
		}
	}
}



public class MutiThreadingExampleWithoutRunnableTwo {

	public static void main(String[] args) {
		ShivamTask shivam=new ShivamTask(); shivam.setName("shivam-thread");
		AmleshTask amlesh=new AmleshTask(); amlesh.setName("amlesh-thread");
		shivam.start(); amlesh.start();

	}

}
