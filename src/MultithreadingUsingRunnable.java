class EvenAdd implements Runnable {
	int sumEven = 0;

	public void run() {
		for (int i = 1; i <= 10; i++) {
			if (i % 2 == 0)
				sumEven = sumEven + i;
			System.out.println("Sum Even : " + sumEven + " By " + Thread.currentThread().getName());
		}
		System.out.println("Sum Of Even Numbers : " + sumEven);
	}
}

class OddAdd implements Runnable {
	public void run() {
		int sumOdd = 0;

		for (int i = 1; i <= 10; i++) {
			if (i % 2 != 0)
				sumOdd = sumOdd + i;
			System.out.println("Sum Odd : " + sumOdd + " By " + Thread.currentThread().getName());
		}
		System.out.println("Sum Of Odd Numbers  : " + sumOdd);
	}
}

public class MultithreadingUsingRunnable {

	public static void main(String[] args) {
		EvenAdd even=new EvenAdd();
		OddAdd odd=new OddAdd();
		Thread evenThread=new Thread(even); evenThread.setName("even-thread");
		Thread oddThread=new Thread(odd); oddThread.setName("odd-thread");
		evenThread.start();
		oddThread.start();
	}

}
