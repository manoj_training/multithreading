import java.util.Scanner;

public class JoinExampleTwo {

	static long s1,s2,s3,s4,total;
	
	public static void main(String[] args) throws Exception {

		Thread t1=new Thread(()->{
			for(int i=1;i<=25000;i++) {
				s1=s1+i;
			}
			System.out.println("SUM#1 : "+s1);
		});
		
		Thread t2=new Thread(()->{
			for(int i=25001;i<=50000;i++) {
				s2=s2+i;
			}
			System.out.println("SUM#2 : "+s2);
		});
		
		Thread t3=new Thread(()->{
			for(int i=50001;i<=75000;i++) {
				s3=s3+i;
			}
			System.out.println("SUM#3 : "+s3);
		});
		
		Thread t4=new Thread(()->{
			for(int i=75001;i<=100000;i++) {
				s4=s4+i;
			}
			System.out.println("SUM#4 : "+s4);
			System.out.println("Give Some Data : ");
			Scanner sc=new Scanner(System.in);
			sc.nextInt();
		});
		
		t1.start(); t2.start(); t3.start(); t4.start();
		t1.join(); t2.join(); t3.join(); t4.join(60000); 
		total=s1+s2+s3+s4;
		System.out.println("TOTAL : "+total);
		Thread tmp=Thread.currentThread();
		System.out.println(tmp.getId());
		System.out.println(tmp.getName());
		System.out.println(tmp.getPriority());
	}

}
