package task;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class FileOperations {

	private String fileName;
	
	public FileOperations(String fileName) {
		this.fileName=fileName;
	}
	
	public void readData() {
		try {
		BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
		while(true) {
			String line=reader.readLine();
			if(line==null)break;
			System.out.println(Thread.currentThread().getName()+"====>"+line);
		}
		reader.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	
}
