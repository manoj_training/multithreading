package task;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

public class MergingData {

	static StringBuffer data1=new StringBuffer();
	static StringBuffer data2=new StringBuffer();
	static StringBuffer data3=new StringBuffer();
	
	public static void main(String[] args) throws Exception {
		
		
		
		Thread t1=new Thread(()->{
			try {
				BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream("d:/iodata/first.txt")));
				while(true) {
					String line=reader.readLine();
					if(line==null)break;
					//System.out.println(line);
					data1.append(line+"\n");
				}
				//System.out.println(data1);
				reader.close();
				}catch(Exception e) {
					e.printStackTrace();
				}
			
		});
		Thread t2=new Thread(()->{
			try {
				BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream("d:/iodata/second.txt")));
				while(true) {
					String line=reader.readLine();
					if(line==null)break;
					//System.out.println(line);
					data2.append(line+"\n");
				}
				//System.out.println(data2);
				reader.close();
				}catch(Exception e) {
					e.printStackTrace();
				}
			
		});
		
		Thread t3=new Thread(()->{
			try {
				BufferedReader reader=new BufferedReader(new InputStreamReader(new FileInputStream("d:/iodata/third.txt")));
				while(true) {
					String line=reader.readLine();
					if(line==null)break;
					//System.out.println(line);
					data3.append(line+"\n");
				}
				//System.out.println(data3);
				reader.close();
				}catch(Exception e) {
					e.printStackTrace();
				}
			
		});
		
		t1.start();
		t2.start();
		t3.start();
		
		t1.join(); t2.join(); t3.join();
		
		
		String data=data1.toString()+data2.toString()+data3.toString();
		FileWriter writer=new FileWriter("d:/iodata/result.txt");
		writer.write(data);
		writer.close();
		System.out.println("Overall Contents Stored To File : \n"+data);
		
		
		
	}

}
