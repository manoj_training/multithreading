package task;

public class MultiFileReader {

	public static void main(String[] args) {
	
		Thread t1=new Thread(new FileOperations("d:/iodata/first.txt")::readData);
		Thread t2=new Thread(new FileOperations("d:/iodata/second.txt")::readData);
		Thread t3=new Thread(new FileOperations("d:/iodata/third.txt")::readData);
		t1.start(); t2.start(); t3.start();
		/*
		Thread t1=new Thread(new Runnable() {
			public void run() {
				FileOperations op1=new FileOperations("d:/iodata/first.txt");
				op1.readData();
			}
		});
		

		Thread t2=new Thread(new Runnable() {
			public void run() {
				FileOperations op2=new FileOperations("d:/iodata/second.txt");
				op2.readData();
			}
		});
		

		Thread t3=new Thread(new Runnable() {
			public void run() {
				FileOperations op3=new FileOperations("d:/iodata/third.txt");
				op3.readData();
			}
		});
		
		t1.start();t2.start();t3.start();
		*/
	
	}

}
