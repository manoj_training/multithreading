package test;

public class SynchronizedMultithreading  
{   
  public static void main(String args[])   
  {   
    Sender sender = new Sender();   
    SenderWThreads sender1 = new SenderWThreads( "Hola " , sender);  
    SenderWThreads sender2 =  new SenderWThreads( "Welcome to Javatpoint website ", sender);  
  
    // Start two threads of SenderWThreads type   
    sender1.start();   
    sender2.start();   
  
    // wait for threads to end   
    try  
    {   
      sender1.join();   
      sender2.join();   
    }   
    catch(Exception e)   
    {   
      System.out.println("Interrupted");   
    }   
  }   
}  