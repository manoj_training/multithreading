package test;

public class DeadLockExampleTwo {

	public static void main(String[] args) {

		String s1 = "INDORE";
		String s2 = "BHOPAL";

		Thread t1 = new Thread(() -> {
			synchronized (s1) {
				for (int i = 0; i < s1.length(); i++) {
					System.out.println(Thread.currentThread().getName() + " S1(" + i + ")=>>>" + s1.charAt(i));
				}
				synchronized (s2) {
					for (int i = 0; i < s2.length(); i++) {
						System.out.println(Thread.currentThread().getName() + " S2(" + i + ")=>>>" + s2.charAt(i));
					}
				}
			}
		});
		
		Thread t2 = new Thread(() -> {
			synchronized (s2) {
				for (int i = 0; i < s2.length(); i++) {
					System.out.println(Thread.currentThread().getName() + " S2(" + i + ")=>>>" + s2.charAt(i));
				}
				synchronized (s1) {
					for (int i = 0; i < s1.length(); i++) {
						System.out.println(Thread.currentThread().getName() + " S1(" + i + ")=>>>" + s1.charAt(i));
					}
				}
			}
		});
		
		t1.setName("First");
		t2.setName("Second");
		t1.start();

		t2.start();

	}

}
