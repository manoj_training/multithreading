
public class MultiThreadingUsingRunnableTwo {

	public static void main(String[] args) throws Exception {
		System.out.println("Main Thread Started....");
		
		Thread shivamThread=new Thread(new Runnable() {
			public void run() {
				for(int i=1; i<=5; i++) {
					System.out.println("Task Getting Done By ...."+Thread.currentThread().getName());
				}
			}
		});
		
		Thread amleshThread=new Thread(new Runnable() {
			public void run() {
				for(int i=1; i<=5; i++) {
					System.out.println("Task Getting Done By ...."+Thread.currentThread().getName());
				}
			
			}
		});
		
		Thread manojThread=new Thread(new Runnable() {
			public void run() {
				for(int i=1; i<=5; i++) {
					System.out.println("Task Getting Done By ...."+Thread.currentThread().getName());
				}
			}
		});
		
		shivamThread.setName("shivam-thread"); amleshThread.setName("amlesh-thread"); manojThread.setName("manoj-thread");
		
		shivamThread.start();
		amleshThread.start();
		manojThread.start();
		
		
		shivamThread.join();
		amleshThread.join();
		manojThread.join();

		System.out.println("Main Thread Completed....");
	}

}
