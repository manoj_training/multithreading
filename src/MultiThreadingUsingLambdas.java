
public class MultiThreadingUsingLambdas {

	public static void main(String[] args) {
		
		Thread amleshThread=new Thread(new ServerReader()::readFromServerOne);
		Thread shivamThread=new Thread(new ServerReader()::readFromServerTwo);
		
		/*
		Runnable shivamRunnable=new ServerReader()::readFromServerOne;
		Runnable amleshRunnable=new ServerReader()::readFromServerTwo;
		
		Thread amleshThread=new Thread(amleshRunnable);
		Thread shivamThread=new Thread(shivamRunnable);
		*/
		
		
		/*
		Thread amleshThread=new Thread(()->{
			for(int i=1; i<=5; i++) {
				System.out.println("Reading From Server#1 By ...."+Thread.currentThread().getName());
			}	
		});
		Thread shivamThread=new Thread(()->{
			for(int i=1; i<=5; i++) {
				System.out.println("Reading From Server#2 By ...."+Thread.currentThread().getName());
			}
		});
		*/
		
		amleshThread.setName("amlesh-thread");
		shivamThread.setName("shivam-thread");
		amleshThread.start();
		shivamThread.start();
		

	}

}
