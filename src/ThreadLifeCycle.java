

class Demo extends Thread{}

public class ThreadLifeCycle {

	public static void main(String[] args) {
		
		Thread t1=new Thread();
		Thread t2=new Thread();
		Thread t3=new Thread();
		Demo t4=new Demo();
		t1.start();
		t2.start();
		t3.start();
		t4.start();

	}

}
