import java.util.Optional;
import java.util.stream.Stream;

public class Demonstration {

	public static void main(String[] args) {
		
		Stream<Integer> stream=Stream.of(10,20,30,40,50);
		Optional<Integer> result=stream.reduce((n1,n2)->n1+n2);
		System.out.println(result.get());
		//stream.filter((n)->n%20==0).forEach(System.out::println);
		//stream.forEach((n)->{int square=n*n; System.out.println(square);}); 

	}

}
