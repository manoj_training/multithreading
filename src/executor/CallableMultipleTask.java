package executor;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableMultipleTask {

	public static void main(String[] args) throws Exception {
		
		ExecutorService service=Executors.newCachedThreadPool();
		
		Callable<Integer> task1=()->{
			int sum=0;
			for(int i=1;i<=100;i++) {
				sum=sum+i;
			}
			return sum;
		};
		Callable<Integer> task2=()->{
			int sum=0;
			for(int i=101;i<=200;i++) {
				sum=sum+i;
			}
			Thread.sleep(25000);
			return sum;
		};
		Callable<Integer> task3=()->{
			int sum=0;
			for(int i=201;i<=300;i++) {
				sum=sum+i;
			}
			return sum;
		};
		
		
		List<Callable<Integer>> allTasks=Arrays.asList(task1,task2,task3);
		List<Future<Integer>> futures=service.invokeAll(allTasks);
		
		for(Future future:futures) {
			System.out.println(future.get());
		}
	
		service.shutdown();
		
		
		

	}

}
