package executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorExampleOne {

	public static void main(String[] args) {
		
		
		//ExecutorService service=Executors.newFixedThreadPool(4);
		//ExecutorService service=Executors.newSingleThreadExecutor();
		ExecutorService service=Executors.newCachedThreadPool();
		service.submit(()->{for(int i=1; i<=5; i++) {System.out.println(Thread.currentThread().getName()+"=>>"+i);}});
		service.submit(()->{for(int i=11; i<=15; i++) {System.out.println(Thread.currentThread().getName()+"=>>"+i);}});
		service.submit(()->{for(int i=21; i<=25; i++) {System.out.println(Thread.currentThread().getName()+"=>>"+i);}});

	
		service.shutdown();
		
		
	}

}
