package executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import java.util.concurrent.Executors;

public class CoreCountExample {

	public static void main(String[] args) {
		
		int n=Runtime.getRuntime().availableProcessors();
		System.out.println("Processors : "+n);
		ExecutorService  service=Executors.newFixedThreadPool(n);
		for(int i=1;i<=10;i++) {
			service.execute(()->{System.out.println(Thread.currentThread().getName());});
		}
		service.shutdown();

		
	}

}
