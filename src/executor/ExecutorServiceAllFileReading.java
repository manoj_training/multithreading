package executor;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import task.FileOperations;

public class ExecutorServiceAllFileReading {

	public static void main(String[] args) {
	
		String folder="d:/iodata";
		File file=new File(folder);
		String names[]=file.list();
		
		ExecutorService service=Executors.newFixedThreadPool(4);
		//ExecutorService service=Executors.newCachedThreadPool();
		
		for(String name:names) {
			String path=folder+"/"+name;
			//System.out.println(path);
			
			Runnable runnable=()->{
				FileOperations op=new FileOperations(path);
				op.readData();
			};
			
			service.submit(runnable);
			
			
		}
		
		service.shutdown();

	}

}
