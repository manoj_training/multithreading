package executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import task.FileOperations;

public class ExecutorServiceExampleFileReading {
	public static void main(String[] args) {
		System.out.println("Reading Files.......");

		Runnable task1=()->{
			FileOperations op=new FileOperations("d:/iodata/first.txt");
			op.readData();
		};
		Runnable task2=()->{
			FileOperations op=new FileOperations("d:/iodata/second.txt");
			op.readData();
		};
		Runnable task3=()->{
			FileOperations op=new FileOperations("d:/iodata/third.txt");
			op.readData();
		};
	
		ExecutorService service=Executors.newCachedThreadPool();
		service.submit(task1); 
		service.submit(task2);
		service.submit(task3);
		
		service.shutdown();
		
	}
}
