package executor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableMultipleTaskTwo {

	static int i=0;
	
	public static void main(String[] args) throws Exception{
	
		ExecutorService service=Executors.newCachedThreadPool();
		
		List<Future<String>> futures=new ArrayList<>();
		
		for(i=1; i<=5; i++) {
			Future<String> future=service.submit(()->{
				System.out.println("Task "+i+" Submitted To "+Thread.currentThread().getName());
				return "Task "+i+" Result : "+new Random().nextInt(100);
			});
			futures.add(future);
		}
		
		for(Future<String> future:futures) {
			System.out.println(future.get());
		}
		
		service.shutdown();
		

	}

}
