package executor;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


class MyTask implements Callable<Integer>{

	@Override
	public Integer call() throws Exception {
		int sum=0;
		for(int i=1;i<=50;i++) {
			sum=sum+i;
		}
		Thread.sleep(20000);
		return sum;
	}
	
}


public class CallableExample {

	public static void main(String[] args) throws Exception, Exception {
		
		ExecutorService service=Executors.newSingleThreadExecutor();
		Future<Integer> future=service.submit(new MyTask());
		System.out.println("Some Other Task-1");
		System.out.println("Some Other Task-2");
		System.out.println("Some Other Task-3");
		Integer result=future.get();
		System.out.println("Result  : "+result);
		System.out.println("Now Operations Over...!");
	}
}
