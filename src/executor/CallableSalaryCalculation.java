package executor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableSalaryCalculation {

	public static void main(String[] args) throws Exception{
		
		List<Emp> employees=Arrays.asList(new Emp(111,"AAA",10000), new Emp(112,"BBB",20000), new Emp(113,"CCC",30000), new Emp(114,"DDD",40000));
		
		ExecutorService service=Executors.newFixedThreadPool(4);
		
		List<Future<Integer>> futures=new ArrayList<>();
		for(Emp emp:employees) {
			Future<Integer> future=service.submit(emp::computeGrossSalary);
			futures.add(future);
		}
		
		System.out.println("Some Unrelated Operations : ");
		System.out.println("Today : "+new java.util.Date());
		System.out.println("Some More Task Not Related With Gross Salary");
		
		System.out.println("Results : ");
		
		for(Future<Integer> future:futures) {
			System.out.println(future.isDone());
			if(future.isDone()==false) {
				future.cancel(false);
			}else {
				System.out.println(future.get());
			}
		}
		
		
		
		service.shutdown();
		
	}

}
