package executor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduleExecutorExampleTwo {

	public static void main(String[] args) {
		System.out.println("Starting Job..........");
		ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
		System.out.println(new java.util.Date());
		Runnable task = () -> {
			System.out.println("Task Started : "+new java.util.Date());
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Task Finished..."+new java.util.Date());
		};
		//service.scheduleAtFixedRate(task, 30, 20, TimeUnit.SECONDS);
		 service.scheduleWithFixedDelay(task, 30, 10, TimeUnit.SECONDS);
		// service.shutdown();
	}

}
