package executor;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import task.FileOperations;

public class ScheduleExecutorExampleOne {

	public static void main(String[] args) {
	
		ScheduledExecutorService service=Executors.newScheduledThreadPool(1);
		Runnable task=new FileOperations("d:/iodata/first.txt")::readData;
		System.out.println("Current Time  : "+new java.util.Date().toString());
		service.schedule(task, 1, TimeUnit.MINUTES);
		
		
		service.shutdown();
		
	}

}
