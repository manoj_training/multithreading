package executor;
import java.util.Scanner;

public class Emp extends Thread {

	private int eno;
	private String name;
	private int basic;
	
	public Emp(int eno, String name, int basic) {
		this.eno=eno; this.name=name; this.basic=basic;
	}
	public int computeGrossSalary() {
		//System.out.println("Basic of "+eno+" : "+basic);
		int hra=basic*20/100;
		int da=basic*10/100;
		int ca=basic*5/100;
		//System.out.println("HRA of "+eno+" : "+hra);
		//System.out.println("DA of "+eno+" : "+da);
		//System.out.println("CA of "+eno+" : "+ca);
		int gross=basic+hra+da+ca;
		if(eno==113) {
			try {
				Thread.sleep(25000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return gross;
	}
	public void processSalary() {
		System.out.println("Basic of "+eno+" : "+basic);
		if(basic>=50000) {
			Scanner sc=new Scanner(System.in);
			System.out.println("Do you want do deduct tax ? ");
			int choice=sc.nextInt();
		}
		System.out.println("HRA of "+eno+" : "+basic*20/100);
		System.out.println("DA of "+eno+" : "+basic*30/100);
		System.out.println("CA of "+eno+" : "+basic*5/100);

	}
	public void run() {processSalary();}
	public static void main(String[] args) {
		Emp e1=new Emp(111,"AAA",10000); Emp e2=new Emp(112,"BBB",50000); Emp e3=new Emp(113,"CCC",30000); Emp e4=new Emp(114,"DDD",40000);
		e1.start(); e2.start(); e3.start();e4.start();
	}
}
